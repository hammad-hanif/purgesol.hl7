﻿(function (module) {

    var userService = function ($http) {

        var changePassword = function (modal) {
            return $http.post("/api/account/changePassword", modal)
                        .then(function (response) {
                            return response.data;
                        })
        }

        return {
            changePassword: changePassword,
        };

    }

    userService.$inject = ["$http"];
    module.factory("userService", userService)

}(angular.module("PurgeSol.HL7.services")))