﻿(function () {

    angular.module("PurgeSol.HL7.controllers", []);
    angular.module("PurgeSol.HL7.services", []);
    angular.module("PurgeSol.HL7.directives", []);
    angular.module("PurgeSol.HL7.interceptors", []);

}());