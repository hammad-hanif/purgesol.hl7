﻿(function () {

    var app = angular.module("PurgeSol.HL7", [
        "PurgeSol.HL7.controllers",
        "PurgeSol.HL7.services",
        "PurgeSol.HL7.directives",
        "PurgeSol.HL7.interceptors",
        "ui.bootstrap",
        "ui.router",
        "toastr",
        "angular-loading-bar",
        "LocalStorageModule",
    ]);

    app.config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state("login", { url: "/login", templateUrl: "/app/templates/login.html", controller: "loginController", data: { cssClassnames: "page-body login-page login-form-fall loaded login-form-fall-init" } })
            .state("me", {
                url: "/me", templateUrl: "/app/templates/menu.html", controller: "menuController", data: { cssClassnames: "page-body skin-cafe gray loaded" }})
                .state("dashboard", { url: "/dashboard", templateUrl: "/app/templates/dashboard.html", controller: "dashboardController", parent: "me" })

        $urlRouterProvider.otherwise("/login");
    });


}())