﻿using System;
using NHapi.Base.Model;
using NHapi.Model.V24.Message;
using NHapi.Model.V24.Datatype;
using PurgeSol.HL7.Model;

namespace PurgeSol.HL7.Parser
{
    public class MessageParser
    {

        public PatientModel ParseA01Message(IMessage message)
        {

            PatientModel model = new PatientModel();

            ADT_A01 msg = message as ADT_A01;

            ID eventType = msg.MSH.MessageType.TriggerEvent;
            TS messageDateTime = msg.MSH.DateTimeOfMessage;

            XCN operatorId = msg.EVN.GetOperatorID(0);
            TS eventOccured = msg.EVN.EventOccurred;

            CX identifier = msg.PID.PatientID;
            XPN pn = msg.PID.GetPatientName(0);
            TS dob = msg.PID.DateTimeOfBirth;
            IS gender = msg.PID.AdministrativeSex;
            XAD address = msg.PID.GetPatientAddress(0);

            IS admissionType = msg.PV1.AdmissionType;

            XCN attendingDoctor=msg.PV1.GetAttendingDoctor(0);
            XCN admittingDoctor = msg.PV1.GetAdmittingDoctor(0);
            XCN referingDoctor = msg.PV1.GetReferringDoctor(0);
            XCN consultingDoctor = msg.PV1.GetConsultingDoctor(0);


            model.Address = ConvertAddress(address);
            model.AdmissionType = admissionType.Value;
            model.AttendingDoctor = ConvertDoctor(attendingDoctor);
            model.AdmittingDoctor = ConvertDoctor(admittingDoctor);
            model.ConsultingDoctor = ConvertDoctor(consultingDoctor);
            model.DateOfBirth = ConvertDate(dob);
            model.EventOccured = ConvertDate(eventOccured);
            model.EventType = eventType.Value;
            model.FirstName = pn.GivenName.Value;
            model.Gender = gender.Value;
            model.OperatorId = operatorId.IDNumber.Value;
            model.RefferingDoctor = ConvertDoctor(referingDoctor);
            model.SurName = pn.FamilyName.Surname.Value;

            return model;
        }

        public PatientModel ParseA02Message(IMessage message)
        {
            PatientModel model = new PatientModel();

            ADT_A01 msg = message as ADT_A01;

            ID eventType = msg.MSH.MessageType.TriggerEvent;
            TS messageDateTime = msg.MSH.DateTimeOfMessage;

            XCN operatorId = msg.EVN.GetOperatorID(0);
            TS eventOccured = msg.EVN.EventOccurred;

            CX identifier = msg.PID.PatientID;
            XPN pn = msg.PID.GetPatientName(0);
            TS dob = msg.PID.DateTimeOfBirth;
            IS gender = msg.PID.AdministrativeSex;
            XAD address = msg.PID.GetPatientAddress(0);

            IS admissionType = msg.PV1.AdmissionType;

            XCN attendingDoctor = msg.PV1.GetAttendingDoctor(0);
            XCN admittingDoctor = msg.PV1.GetAdmittingDoctor(0);
            XCN referingDoctor = msg.PV1.GetReferringDoctor(0);
            XCN consultingDoctor = msg.PV1.GetConsultingDoctor(0);


            model.Address = ConvertAddress(address);
            model.AdmissionType = admissionType.Value;
            model.AttendingDoctor = ConvertDoctor(attendingDoctor);
            model.AdmittingDoctor = ConvertDoctor(admittingDoctor);
            model.ConsultingDoctor = ConvertDoctor(consultingDoctor);
            model.DateOfBirth = ConvertDate(dob);
            model.EventOccured = ConvertDate(eventOccured);
            model.EventType = eventType.Value;
            model.FirstName = pn.GivenName.Value;
            model.Gender = gender.Value;
            model.OperatorId = operatorId.IDNumber.Value;
            model.RefferingDoctor = ConvertDoctor(referingDoctor);
            model.SurName = pn.FamilyName.Surname.Value;

            return model;
        }

        public PatientModel ParseA03Message(IMessage message)
        {
            PatientModel model = new PatientModel();

            ADT_A01 msg = message as ADT_A01;

            ID eventType = msg.MSH.MessageType.TriggerEvent;
            TS messageDateTime = msg.MSH.DateTimeOfMessage;

            XCN operatorId = msg.EVN.GetOperatorID(0);
            TS eventOccured = msg.EVN.EventOccurred;

            CX identifier = msg.PID.PatientID;
            XPN pn = msg.PID.GetPatientName(0);
            TS dob = msg.PID.DateTimeOfBirth;
            IS gender = msg.PID.AdministrativeSex;
            XAD address = msg.PID.GetPatientAddress(0);

            IS admissionType = msg.PV1.AdmissionType;

            XCN attendingDoctor = msg.PV1.GetAttendingDoctor(0);
            XCN admittingDoctor = msg.PV1.GetAdmittingDoctor(0);
            XCN referingDoctor = msg.PV1.GetReferringDoctor(0);
            XCN consultingDoctor = msg.PV1.GetConsultingDoctor(0);


            model.Address = ConvertAddress(address);
            model.AdmissionType = admissionType.Value;
            model.AttendingDoctor = ConvertDoctor(attendingDoctor);
            model.AdmittingDoctor = ConvertDoctor(admittingDoctor);
            model.ConsultingDoctor = ConvertDoctor(consultingDoctor);
            model.DateOfBirth = ConvertDate(dob);
            model.EventOccured = ConvertDate(eventOccured);
            model.EventType = eventType.Value;
            model.FirstName = pn.GivenName.Value;
            model.Gender = gender.Value;
            model.OperatorId = operatorId.IDNumber.Value;
            model.RefferingDoctor = ConvertDoctor(referingDoctor);
            model.SurName = pn.FamilyName.Surname.Value;

            return model;
        }

        public PatientModel ParseA04Message(IMessage message)
        {
            PatientModel model = new PatientModel();

            ADT_A01 msg = message as ADT_A01;

            ID eventType = msg.MSH.MessageType.TriggerEvent;
            TS messageDateTime = msg.MSH.DateTimeOfMessage;

            XCN operatorId = msg.EVN.GetOperatorID(0);
            TS eventOccured = msg.EVN.EventOccurred;

            CX identifier = msg.PID.PatientID;
            XPN pn = msg.PID.GetPatientName(0);
            TS dob = msg.PID.DateTimeOfBirth;
            IS gender = msg.PID.AdministrativeSex;
            XAD address = msg.PID.GetPatientAddress(0);

            IS admissionType = msg.PV1.AdmissionType;

            XCN attendingDoctor = msg.PV1.GetAttendingDoctor(0);
            XCN admittingDoctor = msg.PV1.GetAdmittingDoctor(0);
            XCN referingDoctor = msg.PV1.GetReferringDoctor(0);
            XCN consultingDoctor = msg.PV1.GetConsultingDoctor(0);


            model.Address = ConvertAddress(address);
            model.AdmissionType = admissionType.Value;
            model.AttendingDoctor = ConvertDoctor(attendingDoctor);
            model.AdmittingDoctor = ConvertDoctor(admittingDoctor);
            model.ConsultingDoctor = ConvertDoctor(consultingDoctor);
            model.DateOfBirth = ConvertDate(dob);
            model.EventOccured = ConvertDate(eventOccured);
            model.EventType = eventType.Value;
            model.FirstName = pn.GivenName.Value;
            model.Gender = gender.Value;
            model.OperatorId = operatorId.IDNumber.Value;
            model.RefferingDoctor = ConvertDoctor(referingDoctor);
            model.SurName = pn.FamilyName.Surname.Value;

            return model;
        }

        public PatientModel ParseA05Message(IMessage message)
        {
            PatientModel model = new PatientModel();

            ADT_A01 msg = message as ADT_A01;

            ID eventType = msg.MSH.MessageType.TriggerEvent;
            TS messageDateTime = msg.MSH.DateTimeOfMessage;

            XCN operatorId = msg.EVN.GetOperatorID(0);
            TS eventOccured = msg.EVN.EventOccurred;

            CX identifier = msg.PID.PatientID;
            XPN pn = msg.PID.GetPatientName(0);
            TS dob = msg.PID.DateTimeOfBirth;
            IS gender = msg.PID.AdministrativeSex;
            XAD address = msg.PID.GetPatientAddress(0);

            IS admissionType = msg.PV1.AdmissionType;

            XCN attendingDoctor = msg.PV1.GetAttendingDoctor(0);
            XCN admittingDoctor = msg.PV1.GetAdmittingDoctor(0);
            XCN referingDoctor = msg.PV1.GetReferringDoctor(0);
            XCN consultingDoctor = msg.PV1.GetConsultingDoctor(0);


            model.Address = ConvertAddress(address);
            model.AdmissionType = admissionType.Value;
            model.AttendingDoctor = ConvertDoctor(attendingDoctor);
            model.AdmittingDoctor = ConvertDoctor(admittingDoctor);
            model.ConsultingDoctor = ConvertDoctor(consultingDoctor);
            model.DateOfBirth = ConvertDate(dob);
            model.EventOccured = ConvertDate(eventOccured);
            model.EventType = eventType.Value;
            model.FirstName = pn.GivenName.Value;
            model.Gender = gender.Value;
            model.OperatorId = operatorId.IDNumber.Value;
            model.RefferingDoctor = ConvertDoctor(referingDoctor);
            model.SurName = pn.FamilyName.Surname.Value;

            return model;
        }

        private DateTime ConvertDate(TS dateTime)
        {
            return new DateTime(dateTime.TimeOfAnEvent.Year,
                dateTime.TimeOfAnEvent.Month, dateTime.TimeOfAnEvent.Day, dateTime.TimeOfAnEvent.Hour,
                dateTime.TimeOfAnEvent.Minute, dateTime.TimeOfAnEvent.Second);
        }

        private string ConvertAddress(XAD address)
        {
            return address.StreetAddress+" "+address.City.Value+" "+address.StateOrProvince.Value+" "+address.Country+" "+address.ZipOrPostalCode;
        }

        private PatientDoctorModel ConvertDoctor(XCN doctor)
        {
            PatientDoctorModel model = new PatientDoctorModel();

            model.AssigningAuthority = doctor.AssigningAuthority.NamespaceID.Value;
            model.Degree = doctor.DegreeEgMD.Value;
            model.FirstName = doctor.GivenName.Value;
            model.Id = doctor.IDNumber.Value;
            model.SurName = doctor.FamilyName.Surname.Value;

            return model;
        }
    }
}
