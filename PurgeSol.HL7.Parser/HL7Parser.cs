﻿using NHapi.Base;
using NHapi.Base.Model;
using NHapi.Base.Parser;
using PurgeSol.HL7.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurgeSol.HL7.Parser
{
    public class HL7Parser
    {
        public PatientModel Parse(string message)
        {
            try
            {
                PatientModel patinetModel=new PatientModel();
                var p = new PipeParser();
                Console.WriteLine();
                
                var parsedMessage = p.Parse(message);
                var msgParser = new MessageParser();
                switch (GetStructureName(parsedMessage))
                {
                    case "ADT_A01":
                        patinetModel=msgParser.ParseA01Message(parsedMessage);
                        break;
                    case "ADT_A02":
                        patinetModel = msgParser.ParseA02Message(parsedMessage);
                        break;
                    case "ADT_A03":
                        patinetModel = msgParser.ParseA03Message(parsedMessage);
                        break;
                    case "ADT_A04":
                        patinetModel = msgParser.ParseA04Message(parsedMessage);
                        break;
                    case "ADT_A05":
                        patinetModel = msgParser.ParseA05Message(parsedMessage);
                        break;
                }
                return patinetModel;
            }
            catch (EncodingNotSupportedException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
            catch (HL7Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        private string GetStructureName(IMessage msg)
        {
            return msg.GetStructureName();
        }
    }
}
