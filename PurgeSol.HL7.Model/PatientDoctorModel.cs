﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurgeSol.HL7.Model
{
    public class PatientDoctorModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string Degree { get; set; }
        public string AssigningAuthority { get; set; }
    }
}
