﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurgeSol.HL7.Model
{
    public class PatientModel
    {
        public string EventType { get; set; }

        public string OperatorId { get; set; }
        public Nullable<DateTime> EventOccured { get; set; }

        public string FirstName { get; set; }
        public string SurName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }

        public string AdmissionType { get; set; }
        public PatientDoctorModel AdmittingDoctor { get; set; }
        public PatientDoctorModel AttendingDoctor { get; set; }
        public PatientDoctorModel RefferingDoctor { get; set; }
        public PatientDoctorModel ConsultingDoctor { get; set; }
    }
}
