﻿using System;

namespace PurgeSol.HL7.Listener
{
    public static class ConsoleLogger
    {
        /// <summary>
        /// Write informational event to the console.
        /// </summary>
        /// <param name="message"></param>
        public static void LogInformation(string message)
        {
            Console.WriteLine(DateTime.Now + ": " + message);
        }


        /// <summary>
        /// Write a warning message to the console
        /// </summary>
        /// <param name="message"></param>
        public static void LogWarning(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("WARNING: " + message);
            Console.ResetColor();
        }
    }
}
