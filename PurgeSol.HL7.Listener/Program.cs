﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurgeSol.HL7.Listener
{
    class Program
    {
        private static int port = 5000;
        private static string filePath = "HL7Messages.txt";
        private static bool sendACK = true;
        private static string passthruHost;
        private static int passthruPort;

        static void Main(string[] args)
        {
            HL7TCPListener listener = new HL7TCPListener(port);
            listener.SendACK = sendACK;
            if (filePath != null)
            {
                listener.FilePath = filePath;
            }
            if (passthruHost != null)
            {
                listener.PassthruHost = passthruHost;
                listener.PassthruPort = passthruPort;
            }
            if (!listener.Start())
            {
                ConsoleLogger.LogWarning("Exiting");
            }
        }
    }
}
